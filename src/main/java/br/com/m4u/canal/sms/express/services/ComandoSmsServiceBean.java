package br.com.m4u.canal.sms.express.services;

import br.com.m4u.canal.sms.express.bean.Comando;

/**
 * Created by diogo.nobre on 26/10/2016.
 */
public interface ComandoSmsServiceBean {

        public Comando buscarComando(String comando) ;
}