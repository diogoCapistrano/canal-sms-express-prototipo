package br.com.m4u.canal.sms.express.enums;

import br.com.m4u.canal.sms.express.Contexto;

import java.util.List;

/**
 * Created by diogo.nobre on 26/10/2016.
 */
public enum ComandoStrategyEnum {
        ADESAO {

                public void execute(Contexto contexto) {
                        System.out.println("Executa a classe CadastrarClienteComand");
                }
        },
        MIGRACAO {
                public void execute(Contexto contexto) {
                        System.out.println("Executa a classe AlterarProdutoComand");
                }
        },
        ALTERACAO_PLUGIN {
                public void execute(Contexto contexto) {
                        System.out.println("Executa a classe AlterarPluginComand");
                }
        },
        /**
         * TODO: Rever o nome desse enum não está muito intuitivo, pq isso eh uma operacao que soh verifica a elegibilidade do cliente
         * e envia uma mensagem para ele de sucesso ou de negacao
         */

        ENVIAR_MENSAGEM {
                public void execute(Contexto contexto) {
                        System.out.println("Executa a classe EnviarMensagemComand");
                }
        },
        RECONTRATACAO {
                public void execute(Contexto contexto) {
                        System.out.println("Executa a classe RecontratarPacoteDadosCommand");
                }
        };

        private List<String> comandos;

        public abstract void execute(Contexto contexto);
}