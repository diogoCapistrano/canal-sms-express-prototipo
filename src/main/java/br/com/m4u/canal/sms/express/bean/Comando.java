package br.com.m4u.canal.sms.express.bean;

/**
 * Created by diogo.nobre on 26/10/2016.
 */
public class Comando {
        String tipoComando;
        String mensagemErro;
        String mensagemSucesso;

        public String getTipoComando() {
                return tipoComando;
        }

        public void setTipoComando(String tipoComando) {
                this.tipoComando = tipoComando;
        }

        public String getMensagemErro() {
                return mensagemErro;
        }

        public void setMensagemErro(String mensagemErro) {
                this.mensagemErro = mensagemErro;
        }

        public String getMensagemSucesso() {
                return mensagemSucesso;
        }

        public void setMensagemSucesso(String mensagemSucesso) {
                this.mensagemSucesso = mensagemSucesso;
        }
}