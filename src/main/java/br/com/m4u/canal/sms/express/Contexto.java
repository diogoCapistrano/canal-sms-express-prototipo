package br.com.m4u.canal.sms.express;

import br.com.m4u.canal.sms.express.bean.Comando;

/**
 * Created by diogo.nobre on 26/10/2016.
 */
public class Contexto {
        String cvv;
        String msisdn;
        Comando comando;

        public Contexto(String msisdn, Comando comando) {
                this.msisdn = msisdn;
                this.comando = comando;
        }
}