package br.com.m4u.canal.sms.express;

import br.com.m4u.canal.sms.express.bean.Comando;
import br.com.m4u.canal.sms.express.services.ComandoSmsServiceBean;

import java.util.regex.Pattern;

/**
 * Created by diogo.nobre on 26/10/2016.
 */
public class ComandoInterprete {

        private String cvv;
        private String mensagemRecebida;
        private String sohComando = new String();
        private ComandoSmsServiceBean comandoSmsServiceBean;

        /**
         * M&eacute;todo que verifica se a palavra &eacute; um comando sms ou n&atilde;o. A regra &eacute;:
         * se s&oacute; tiver n&uacute;mero o sistema interpreta como cvv.
         * @param palavra par&acirc;metro passado na mensagem sms pelo cliente.
         * @return falso se a palavra passada tiver só números.
         */
        private boolean ehComando(String palavra) {
                return Pattern.matches("^[0-9]*$", palavra);
        }

        private void separarComandoDoCvv() {
                String[] palavrasDaMensagemRecebida = this.mensagemRecebida.split(" ");
                for (String palavra : palavrasDaMensagemRecebida) {
                        if(this.ehComando(palavra)) {
                                this.sohComando.concat(palavra + " ");
                        } else {
                                this.cvv = palavra;
                        }
                }
                this.sohComando = this.sohComando.trim();
        }

        public ComandoInterprete(String mensagemRecebida) {
                this.mensagemRecebida = mensagemRecebida;
        }

        public Comando interpretarComando() {
                this.separarComandoDoCvv();
                return this.comandoSmsServiceBean.buscarComando(this.sohComando);
        }

        public String getSohComando() {
                return this.sohComando;
        }

        public String getCvv() {
                return cvv;
        }
        
}