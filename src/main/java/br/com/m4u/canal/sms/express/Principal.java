package br.com.m4u.canal.sms.express;

import br.com.m4u.canal.sms.express.bean.Comando;
import br.com.m4u.canal.sms.express.enums.ComandoStrategyEnum;

import java.util.regex.Pattern;

/**
 * Created by diogo.nobre on 26/10/2016.
 */
public class Principal {

        public static void main(String[] args) {
                String sms = "LIGHT";
                Comando comando = new ComandoInterprete(sms).interpretarComando();
                Contexto contexto = new Contexto("5521999999999", comando);
                ComandoStrategyEnum.valueOf(comando.getTipoComando()).execute(contexto);
        }
}